# FFTXlib

Implements real space grid parallelization of FFT and task groups. 

## Installation with CMake with internal FFTS 

*  create a build directory and cd into it 
```
mkdir build 
cd build 
``` 

* use cmake command to create your build environment.
``` 
cmake  ../ -DQE_FFTW_VENDOR=Internal  -DCMAKE_INSTALL_PREFIX=<install-path>  -DQE_ENABLE_OPENMP=1
```
* Compile the library and install it with make:  
```
make 
make install 
```

## Link with external FFT libraries 
  * Intel DFTI -DQE_FFTW_VENDOR=Intel_DFTI 
  * Intel FFTW3 -DQE_FFTW_VENDOR=FFTW3 


# To do list 
* include GPU support NVidia, AMD, GPU 
* extend cmake 
* add compilation and execution of tests. 

